import unittest
from tkinter import *
from automate import verifivation_positive, verification_afforestation, verification_rule, update_color, creation_rectangle,  check_neighbour, set_fire, check_neighbour_moore

class TestAutomate(unittest.TestCase):

    def setUp(self):
        #tk & canvas
        self.master = Tk()
        canvas_width = 1920
        canvas_height = 1080
        self.canvas = Canvas(self.master, width=canvas_width, height=canvas_height)
        #liste de rectangle
        # grey  | red   | green | white
        # white | green | green | white
        self.list_rectangle = []
        self.list_rectangle.append(self.canvas.create_rectangle(0, 0, 1, 1, fill = 'grey'))
        self.list_rectangle.append(self.canvas.create_rectangle(0, 0, 1, 1, fill = 'red'))
        self.list_rectangle.append(self.canvas.create_rectangle(0, 0, 1, 1, fill = 'green'))
        self.list_rectangle.append(self.canvas.create_rectangle(0, 0, 1, 1, fill = 'white'))
        self.list_rectangle.append(self.canvas.create_rectangle(0, 0, 1, 1, fill = 'white'))
        self.list_rectangle.append(self.canvas.create_rectangle(0, 0, 1, 1, fill = 'green'))
        self.list_rectangle.append(self.canvas.create_rectangle(0, 0, 1, 1, fill = 'green'))
        self.list_rectangle.append(self.canvas.create_rectangle(0, 0, 1, 1, fill = 'white'))
        self.rows = 2
        self.cols = 4

        #liste de sauvegarde des couleurs
        self.list_color = []
        self.list_color.append('grey')
        self.list_color.append('red')
        self.list_color.append('green')
        self.list_color.append('white')
        self.list_color.append('white')
        self.list_color.append('green')
        self.list_color.append('green')
        self.list_color.append('white')
    
    def test_verifivation_positive(self):
        self.assertTrue(verifivation_positive(1))
        self.assertTrue(verifivation_positive(5))
        self.assertTrue(verifivation_positive(6.7))
        self.assertFalse(verifivation_positive(0))
        self.assertFalse(verifivation_positive(-1))
        self.assertFalse(verifivation_positive(-2.4))

    def test_verification_afforestation(self):
        self.assertTrue(verification_afforestation(0))
        self.assertTrue(verification_afforestation(1))
        self.assertTrue(verification_afforestation(0.98))
        self.assertTrue(verification_afforestation(0.67543))
        self.assertFalse(verification_afforestation(-0.67543))
        self.assertFalse(verification_afforestation(-1))
        self.assertFalse(verification_afforestation(2))
        self.assertFalse(verification_afforestation(2.87))

    def test_verification_rule(self):
        self.assertTrue(verification_rule(2))
        self.assertTrue(verification_rule(1))
        self.assertFalse(verification_rule(1.5))
        self.assertFalse(verification_rule(5))
        self.assertFalse(verification_rule(0))
        self.assertFalse(verification_rule(-1.5))

    def test_check_neighbour(self):
        #Von Neumann
        self.assertTrue(check_neighbour(self.list_rectangle, self.list_color, self.list_rectangle[2], 2, self.rows, self.cols, self.canvas, 1, 1))
        self.assertTrue(check_neighbour(self.list_rectangle, self.list_color, self.list_rectangle[5], 5, self.rows, self.cols, self.canvas, 1, 1))
        self.assertFalse(check_neighbour(self.list_rectangle, self.list_color, self.list_rectangle[6], 6, self.rows, self.cols, self.canvas, 1, 1))
        self.assertFalse(check_neighbour(self.list_rectangle, self.list_color, self.list_rectangle[7], 7, self.rows, self.cols, self.canvas, 1, 1))
        #Moore
        self.assertTrue(check_neighbour(self.list_rectangle, self.list_color, self.list_rectangle[2], 2, self.rows, self.cols, self.canvas, 1, 2))
        self.assertTrue(check_neighbour(self.list_rectangle, self.list_color, self.list_rectangle[5], 5, self.rows, self.cols, self.canvas, 1, 2))
        self.assertTrue(check_neighbour(self.list_rectangle, self.list_color, self.list_rectangle[6], 6, self.rows, self.cols, self.canvas, 1, 2))
        self.assertFalse(check_neighbour(self.list_rectangle, self.list_color, self.list_rectangle[7], 7, self.rows, self.cols, self.canvas, 1, 2))
    
    def test_check_neighbour_Moore(self):
        self.assertEqual(check_neighbour_moore(self.list_rectangle, self.list_color, self.list_rectangle[6], 6, self.rows, self.cols, self.canvas), 1)
        self.assertEqual(check_neighbour_moore(self.list_rectangle, self.list_color, self.list_rectangle[5], 5, self.rows, self.cols, self.canvas), 0)
        self.assertEqual(check_neighbour_moore(self.list_rectangle, self.list_color, self.list_rectangle[2], 2, self.rows, self.cols, self.canvas), 0)

    def test_set_fire(self):
        set_fire(self.list_rectangle[7], self.canvas, 1, 1)
        self.assertEqual(self.canvas.itemcget(self.list_rectangle[7], "fill"), "red")

    def test_creation_rectangle(self):
        rectangles = creation_rectangle(self.canvas, 5, 5, 1, 0.5)
        self.assertEqual(len(rectangles), 25)
        coord = self.canvas.coords(rectangles[3])
        self.assertEqual(coord[2]-coord[0], 1)
        self.assertEqual(coord[3]-coord[1], 1)
        rectangles = creation_rectangle(self.canvas, 2, 11, 4, 0.5)
        self.assertEqual(len(rectangles), 22)
        coord = self.canvas.coords(rectangles[0])
        self.assertEqual(coord[2]-coord[0], 4)
        self.assertEqual(coord[3]-coord[1], 4)
        self.assertEqual(coord[0], 0)
        self.assertEqual(coord[1], 0)
        coord = self.canvas.coords(rectangles[13])
        self.assertEqual(coord[0], 8)
        self.assertEqual(coord[1], 4)

    #On fait deux jeux de tests pour update_color
    # 1 pour Moore et 1 pour Von Neumann pour que la liste des rectangles du setup reset entre les tests.
    def test_update_color_vonneumann(self):
        self.assertTrue(update_color(self.list_rectangle, self.list_color, self.list_rectangle[0], 0, self.rows, self.cols, self.canvas, 1, 1))
        self.assertEqual(self.canvas.itemcget(self.list_rectangle[0], "fill"), 'white')
        self.assertTrue(update_color(self.list_rectangle, self.list_color, self.list_rectangle[1], 1, self.rows, self.cols, self.canvas, 1, 1))
        self.assertEqual(self.canvas.itemcget(self.list_rectangle[1], "fill"), 'grey')
        self.assertTrue(update_color(self.list_rectangle, self.list_color, self.list_rectangle[2], 2, self.rows, self.cols, self.canvas, 1, 1))
        self.assertEqual(self.canvas.itemcget(self.list_rectangle[2], "fill"), 'red')
        self.assertFalse(update_color(self.list_rectangle, self.list_color, self.list_rectangle[3], 3, self.rows, self.cols, self.canvas, 1, 1))
        self.assertEqual(self.canvas.itemcget(self.list_rectangle[3], "fill"), 'white')
        self.assertFalse(update_color(self.list_rectangle, self.list_color, self.list_rectangle[4], 4, self.rows, self.cols, self.canvas, 1, 1))
        self.assertEqual(self.canvas.itemcget(self.list_rectangle[4], "fill"), 'white')
        self.assertTrue(update_color(self.list_rectangle, self.list_color, self.list_rectangle[5], 5, self.rows, self.cols, self.canvas, 1, 1))
        self.assertEqual(self.canvas.itemcget(self.list_rectangle[5], "fill"), 'red')
        self.assertFalse(update_color(self.list_rectangle, self.list_color, self.list_rectangle[6], 6, self.rows, self.cols, self.canvas, 1, 1))
        self.assertEqual(self.canvas.itemcget(self.list_rectangle[6], "fill"), 'green')
        self.assertFalse(update_color(self.list_rectangle, self.list_color, self.list_rectangle[7], 7, self.rows, self.cols, self.canvas, 1, 1))
        self.assertEqual(self.canvas.itemcget(self.list_rectangle[7], "fill"), 'white')

    def test_update_color_moore(self):
        self.assertTrue(update_color(self.list_rectangle, self.list_color, self.list_rectangle[0], 0, self.rows, self.cols, self.canvas, 1, 2))
        self.assertEqual(self.canvas.itemcget(self.list_rectangle[0], "fill"), 'white')
        self.assertTrue(update_color(self.list_rectangle, self.list_color, self.list_rectangle[1], 1, self.rows, self.cols, self.canvas, 1, 2))
        self.assertEqual(self.canvas.itemcget(self.list_rectangle[1], "fill"), 'grey')
        self.assertTrue(update_color(self.list_rectangle, self.list_color, self.list_rectangle[2], 2, self.rows, self.cols, self.canvas, 1, 2))
        self.assertEqual(self.canvas.itemcget(self.list_rectangle[2], "fill"), 'red')
        self.assertFalse(update_color(self.list_rectangle, self.list_color, self.list_rectangle[3], 3, self.rows, self.cols, self.canvas, 1, 2))
        self.assertEqual(self.canvas.itemcget(self.list_rectangle[3], "fill"), 'white')
        self.assertFalse(update_color(self.list_rectangle, self.list_color, self.list_rectangle[4], 4, self.rows, self.cols, self.canvas, 1, 2))
        self.assertEqual(self.canvas.itemcget(self.list_rectangle[4], "fill"), 'white')
        self.assertTrue(update_color(self.list_rectangle, self.list_color, self.list_rectangle[5], 5, self.rows, self.cols, self.canvas, 1, 2))
        self.assertEqual(self.canvas.itemcget(self.list_rectangle[5], "fill"), 'red')
        self.assertTrue(update_color(self.list_rectangle, self.list_color, self.list_rectangle[6], 6, self.rows, self.cols, self.canvas, 1, 2))
        self.assertEqual(self.canvas.itemcget(self.list_rectangle[6], "fill"), 'red')
        self.assertFalse(update_color(self.list_rectangle, self.list_color, self.list_rectangle[7], 7, self.rows, self.cols, self.canvas, 1, 2))
        self.assertEqual(self.canvas.itemcget(self.list_rectangle[7], "fill"), 'white')

    def tearDown(self):
        pass