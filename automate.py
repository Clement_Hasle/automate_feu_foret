from tkinter import *
from tkinter import messagebox
import datetime
import random
import argparse
from math import *

#Marge sous la grille
BUTTON_MARGIN = 100

def bind_click_rectangle(event, canvas):
    """Fonction de bind pour mettre en feu les arbres ou annuler.
    IN  : canvas pour manipuler les rectangles
    """
    rectangle = canvas.find_overlapping(event.x, event.y, event.x, event.y)
    if(canvas.itemcget(rectangle[0], "fill") == "green"):
        canvas.itemconfig(rectangle[0], fill="red")
    elif(canvas.itemcget(rectangle[0], "fill") == "red"):
            canvas.itemconfig(rectangle[0], fill="green")

def verifivation_positive(nb):
    """Vérifie qu'un nombre est positif.
    IN  : nb est le nombre à vérifier.
    OUT : booléen égale à vrai si nb est positif et faux sinon.
    """
    if(nb>0):
        return True
    return False

def verification_afforestation(nb):
    """Vérifie qu'un nombre est entre 0 et 1.
    IN  : nb est le nombre à vérifier.
    OUT : booléen égale à vrai si nb entre 0 et 1 compris et faux sinon.
    """
    if nb <= 1 and nb >= 0:
        return True
    return False

def verification_rule(rule):
    """Vérifie qu'un nombre est égale à 1 ou 2.
    IN  : nb est le nombre à vérifier.
    OUT : booléen égale à vrai si nb est égale à 1 ou 2 et faux sinon.
    """
    if rule == 1 or rule == 2:
        return True
    return False

def verification_grid_size(master, rows, cols, cell_size):
    """Vérifie que la grille à créer ne dépasse pas la résolution de l'écran de l'utilisateur.
    IN  : master est la fenêtre tKinter contenant la grille.
        : rows est le nombre de lignes de la grille.
        : cols est le nombre de colonnes de la grille.
        : cell_size est la taille d'une cellule de la grille.
    OUT : booléen égale à vrai si la grille ne dépasse pas l'écran et faux sinon.
    """
    screen_width = master.winfo_screenwidth()
    screen_height = master.winfo_screenheight()
    if cols*cell_size < screen_width and rows*cell_size + BUTTON_MARGIN < screen_height:
        return True
    return False

def check_neighbour(list_rectangle, list_color, rectangle, index, rows, cols, canvas, rule, rule_neighbour):
    """Vérifie si les quatres voisins adjacents sont en feu ou non. Si oui, on appelle la fonction set_fire pour mettre en feu l'arbre.
    Si la règle de voisinage est 2 = Moore, appelle check_neighbour_moore pour vérifier les voisins en diagonal.
    IN  : list_rectangle répértorie tous les rectangles.
        : list_color est la liste des couleurs des rectangles de la grille (sert de sauvegarde avant changement).
        : rectangle est le rectangle à mettre à jour.
        : index est la position du rectangle dans la liste de rectangle composant la grille.
        : rows est le nombre de lignes de la grille.
        : cols est le nombre de colonnes de la grille.
        : canvas est le canvas contenant les rectangles.
        : rule est la règle de calcul pour la transition du feu (1 ou 2).
        : rule_neighbour est la règle de voisinage (1 ou 2)
    OUT : booléen égale à vrai si un arbre a été mis en feu et faux sinon.
    """
    line = floor(index/cols)
    count = 0
    if rule_neighbour == 2:
        count += check_neighbour_moore(list_rectangle, list_color, rectangle, index, rows, cols, canvas)
    if(index - cols >= 0):
        if(list_color[index - cols] == 'red'):
            count = count + 1
    if(index - 1 >= 0 and floor((index - 1)/cols) == line):
        if(list_color[index-1] == 'red'):
            count = count + 1
    if(index + 1 <= (rows*cols)-1 and floor((index + 1)/cols) == line):
        if(list_color[index+1] == 'red'):
            count = count + 1
    if(index + cols <= (rows*cols)-1):
        if(list_color[index + cols] == 'red'):
            count = count + 1
    if(count > 0):
        return set_fire(rectangle, canvas, rule, count)
    else:
        return False

def check_neighbour_moore(list_rectangle, list_color, rectangle, index, rows, cols, canvas):
    """Vérifie si les quatres voisins diagonaux sont en feu ou non. Renvoie le nombre de voisins en diagonal en feu.
    IN  : list_rectangle répértorie tous les rectangles.
        : list_color est la liste des couleurs des rectangles de la grille (sert de sauvegarde avant changement).
        : rectangle est le rectangle à mettre à jour.
        : index est la position du rectangle dans la liste de rectangle composant la grille.
        : rows est le nombre de lignes de la grille.
        : cols est le nombre de colonnes de la grille.
        : canvas est le canvas contenant les rectangles.
    OUT : count est le nombre de voisins en diagonal du rectangle étant en feu.
    """
    line_sup = floor((index + cols)/cols)
    line_inf = floor((index - cols)/cols)
    count = 0
    if(index - cols -1 >= 0 and floor((index - cols - 1)/cols) == line_inf):
        if(list_color[index - cols - 1] == 'red'):
            count = count + 1
    if(index - cols + 1 >= 0 and floor((index - cols + 1)/cols) == line_inf):
        if(list_color[index - cols + 1] == 'red'):
            count = count + 1
    if(index + cols - 1 <= (rows*cols)-1 and floor((index + cols - 1)/cols) == line_sup):
        if(list_color[index + cols - 1] == 'red'):
            count = count + 1
    if(index + cols + 1 <= (rows*cols)-1 and floor((index + cols + 1)/cols) == line_sup):
        if(list_color[index + cols + 1] == 'red'):
            count = count + 1
    return count

def set_fire(rectangle, canvas, rule, count):
    """Permet de mettre le feu à un arbre selon la règle choisie.
    IN  : rectangle est le rectangle à mettre en feu.
        : canvas est le canvas contenant les rectangles.
        : rule est la règle de calcul pour la transition du feu (1 ou 2).
        : count est le nombre de voisin en feu.
    OUT : booléen égale à vrai si la case est devenu rouge et faux sinon.
    """
    if(rule==1):
        canvas.itemconfig(rectangle, fill="red")
        return True
    else:
        chance = 1 - (1/(count+1))
        if(random.random() <= chance):
            canvas.itemconfig(rectangle, fill="red")
            return True
        return False


def creation_rectangle(canvas, rows, cols, cell_size, afforestation):
    """Crée la grille de rectangles dans le canvas et bind le click gauche.
    Retourne la liste des rectangles créés.
    IN  : canvas est le canvas contenant les rectangles.
        : rows est le nombre de lignes de la grille.
        : cols est le nombre de colonnes de la grille.
        : cell_size est la taille des cellules de la grille
        : afforestation est le taux de boisement de la grille      
    OUT : liste des rectangles composant la grille.
    """
    list_rectangle = []
    x = 0
    y = 0
    for index in range(0, rows*cols):
        if random.random() > afforestation:
            color = 'white'
        else:
            color = 'green'
        list_rectangle.append(canvas.create_rectangle(x, y, x + cell_size, y + cell_size, fill = color))
        x += cell_size
        if x >= cell_size*cols:
            x = 0
            y += cell_size
    canvas.bind('<Button-1>', lambda event, a = canvas: bind_click_rectangle(event, a))
    return list_rectangle

def update_color(list_rectangle, list_color, rectangle, index, rows, cols, canvas, rule, rule_neighbour):
    """Met à jour la couleur d'un rectangle dans la grille. Appelle check_neighbour si celui-ci est vert.
    IN  : list_rectangle est la liste de rectangles composant la grille.
        : list_color est la liste des couleurs des rectangles de la grille (sert de sauvegarde avant changement).
        : rectangle est le rectangle à mettre à jour.
        : index est la position du rectangle dans la liste de rectangles.
        : rows est le nombre de lignes de la grille.
        : cols est le nombre de colonnes de la grille.
        : canvas est le canvas contenant les rectangles.
        : rule est la règle de calcul pour la transition du feu (1 ou 2).
        : rule_neighbour est la règle de voisinage (1 ou 2).
    OUT : has_changed est un booléen égale à vrai si le rectangle a changé de couleur et faux sinon.
    """
    has_changed = True
    if canvas.itemcget(rectangle, "fill") == 'green':
        has_changed = check_neighbour(list_rectangle, list_color, rectangle, index, rows, cols, canvas, rule, rule_neighbour)
    else:
        if canvas.itemcget(rectangle, "fill") == 'red':
            canvas.itemconfig(rectangle, fill="grey")
        else:
            if canvas.itemcget(rectangle, "fill") == 'grey':
                canvas.itemconfig(rectangle, fill="white")
            else:
                has_changed = False
    return has_changed

def anim(master, duration, list_rectangle, rows, cols, rule, canvas, validation_button, rule_neighbour):
    """Lance la simulation. Crée la liste de couleurs et appelle update_color pour chaque rectangle de la grille.
    Si un changement c'est produit, anim s'appelle. Modifie le bouton de validation pour pouvoir quitter et affiche
    un messagebox lors de la fin de la simulation.
    IN  : master est la fenêtre tkinter.
        : duration est la durée de l'animation.
        : list_rectangle est la liste de rectangle de la grille.
        : rows est le nombre de lignes de la grille.
        : cols est le nombre de colonnes de la grille.
        : rule est la règle de calcul pour la transition du feu (1 ou 2).
        : canvas est le canvas contenant les rectangle.
        : validation_button est le bouton de validation à modifier.
        : rule_neighbour est la règle de voisinage (1 ou 2).
    """
    canvas.unbind("<Button 1>")
    validation_button['text'] = 'quitter'
    validation_button['command'] = master.destroy
    change = False
    list_color = [] 
    for rectangle in list_rectangle:
        list_color.append(canvas.itemcget(rectangle, "fill"))
    rectangle_position = 0
    for rectangle in list_rectangle:
        if(update_color(list_rectangle, list_color, rectangle, rectangle_position, rows, cols, canvas, rule, rule_neighbour)):
            change = TRUE
        rectangle_position = rectangle_position + 1
    if(change):
        master.after(duration, lambda: anim(master, duration, list_rectangle, rows, cols, rule, canvas, validation_button, rule_neighbour))
    else:
        messagebox.showinfo("Fin", "La Simulation terminée.")

if __name__=="__main__":
    parser = argparse.ArgumentParser(description="Enter rows, cols, cell_size, afforestation, duration, rule")
    parser.add_argument("rows", help="Nb de ligne de la grille")
    parser.add_argument("cols", help="Nb de colonne de la grille")
    parser.add_argument("cell_size", help="Taille des cellules")
    parser.add_argument("afforestation", help="Taux de boisement")
    parser.add_argument("duration", help="Durée de l'animation")
    parser.add_argument("rule", help="Règle de transition")
    parser.add_argument("rule_neighbour", help="Règle de voisinage (1 = Moore, 2 = Von Neumann)")
    args = parser.parse_args()

    master = Tk()

    if verifivation_positive(int(args.rows)) and verifivation_positive(int(args.rows)) and verifivation_positive(int(args.cell_size)) and verifivation_positive(float(args.duration)):
        if verification_grid_size(master, int(args.rows), int(args.cols), int(args.cell_size)):
            if verification_afforestation(float(args.afforestation)):
                if verification_rule(int(args.rule)) and verification_rule(int(args.rule_neighbour)):
                    master.rowconfigure(2, weight=1)
                    master.columnconfigure(1, weight=1)
                    list_rectangle = []
                    validation_button = Button(master, text="Valider", command = lambda: anim(master, int(float(args.duration)*1000), list_rectangle, int(args.rows), int(args.cols), int(args.rule), canvas, validation_button, int(args.rule_neighbour)))
                    validation_button.grid(column=0, row=1)

                    canvas_width = int(args.cols)*int(args.cell_size)
                    canvas_height = int(args.rows)*int(args.cell_size)
                    canvas = Canvas(master, width=canvas_width, height=canvas_height)
                    canvas.grid(column=0, row=0)
                    list_rectangle = creation_rectangle(canvas, int(args.rows), int(args.cols), int(args.cell_size), float(args.afforestation))
                else:
                    messagebox.showinfo("Erreur", "La règle de calcul de transition du feu et la règle de voisinage doivent être égales à 1 ou 2 (1 : règle de calcul simple, 2 : en fonction du nombre de voisins et 1 : Moore, 2 : Von Neumann)")
                    master.destroy()
            else:
                messagebox.showinfo("Erreur", "Le taux de boisement doit être compris entre 0 et 1.")
                master.destroy()
        else:
            messagebox.showinfo("Erreur", "La taille de la grille dépasse l'écran avec les paramètres sélectionnés.")
            master.destroy()
    else:
        messagebox.showinfo("Erreur", "Le nombre de colonnes, de lignes, la taille des cellules ainsi que la durée d'animation doivent être strictement positifs.")
        master.destroy()

    master.mainloop()