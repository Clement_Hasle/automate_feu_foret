# CONTENT
Projet automate feu de forêt M3301 IUT Amiens
# AUTHORS
GABET Benjamin & HASLE Clément
# Call example
The following parameters are required when executing the program :<br/><br/>

- rows : number of rows in the simulation<br/>
- cols : number of columns in teh simultaion<br/>
- cell_size : size of cells<br/>
- afforestation : afforestation rate<br/>
- duration : duration of one turn (seconds)<br/>
- rule : calculation rule to make a cell catch fire <br/>
    1 = simple calculation rule, if a neighbor cell is on fire, set fire next turn<br/>
    2 = calculate the chance to catch fire according to the number of neighbors on fire<br/>
- rule_neighbour : rule to know which neighbor must be taken in consideration when catching fire.<br/>
    1 : Moore rule (horizontal and vertical)<br/>
    2 : Von Neumann rule (horizontal, vertical and diagonal)<br/>
<br/>
example of call :<br/><br/>
    python .\automate.py 20 30 25 0.55 1 2 2<br/><br/>
For this example we are in the project depository and you might need to use the command python3 on Unix OS.<br/>